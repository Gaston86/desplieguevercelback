// Express es lo que nos permite recibir las solicitudes y manejar las respuestas. Nos traemos el modulo de express, luego creamos una variable que contenga ese modulo y definimos el puerto.
const express = require('express');

// morgan es un registrador de solicitudes http (su enfoque es registrar solo cosas relacionadas con http)
const logger = require('morgan');

// body-parser es un middleware que verifica y valida las solicitudes de cuerpo entrantes (para que coincidan con lo que esperamos)
const bodyParser = require('body-parser');

//Express es la parte de node que gestiona lo req y hacemos useo a traves de la variable app
const app = express();

//prueba back 3

//BackPruebaVercel

const dbConection = require('./configuracion/config')

// Registro de solicitudes por consola.
app.use(logger('dev'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false })); // Accepts urlencoded body

app.listen(3001);

  //Require our routes into the application.
    require('./routes/index')(app);

//comentamos ya que esto se encuentra en ingresosTodo 
//Definimos un enpoint de tipo get apuntando a la lista de usuarios, ejecutamos un query a la bd mediante dbConection y a su vez se le ejecuta una funcion de callbak con los param de error y rows(devuelve los resultados)
/*   app.get ('/usuarios', (req,res)=>{
    dbConection.query("select * from usuarios", (errores,rows)=>{
      if (!errores){
        //si no hay errores devuelvo el error 200 q significa que esta todo bien y devuelve no lo que halla en la var rows//
        res.status(200).send(rows);
      } else{
        //en caso de errores devuelve el estado 400, y devuelve el error ya definido en la dependencia//
        res.status(400).send(errores);
      }
  })
}) */

/* app.get ('/ingresos', (req,res)=>{
  dbConection.query("select * from ingresos", (errores,rows)=>{
    if (!errores){
      //si no hay errores devuelvo el error 200 q significa que esta todo bien y devuelve no lo que halla en la var rows//
      res.status(200).send(rows);
    } else{
      //en caso de errores devuelve el estado 400, y devuelve el error ya definido en la dependencia//
      res.status(400).send(errores);
    }
})
}) */

/* app.get ('/egresos', (req,res)=>{
  dbConection.query("select * from egresos", (errores,rows)=>{
    if (!errores){
      //si no hay errores devuelvo el error 200 q significa que esta todo bien y devuelve no lo que halla en la var rows//
      res.status(200).send(rows);
    } else{
      //en caso de errores devuelve el estado 400, y devuelve el error ya definido en la dependencia//
      res.status(400).send(errores);
    }
})
}) */


// vamos a estar recibiendo un req get en el enpoint o URI que seria la "/" que seria la pag principal, como seg parametros tenemos una funcion de callback.
//app.get('/', (req, res) => {
 //   res.send('Hello')
  //})
  

  module.exports = app;